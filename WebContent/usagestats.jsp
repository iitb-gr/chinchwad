<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="js/jquery-1.11.3.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Usage Stats</title>
<style>
body{
	background-image: url("img.png");
}
a:link {
    text-decoration: none;
}
th{
	background-color : rgba(248, 247, 216, 0.7);
}
tr{
	background-color: rgba(248, 247, 216, 0.7);
}
table, td, th {
	
    border: 1px solid green;
}
td {
    padding: 15px;
}
th {
	padding: 20px;
    color: black;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
	$(window).load(function() {
		$.ajax({
	       type : "GET",
	       url : "http://ruralict.cse.iitb.ac.in/Chinchwad/UsageStats",
	       success : function(data) {	 
	    	   $.each(data,function(index){	   
	    	    		   
	    		   $("#r1").css('color','#0000FF').append(data[index].total_count);
	    		   $("#r2").css('color','green').append(data[index].accept_count);
	    		   $("#r3").css('color','red').append(data[index].reject_count);
	    		   $("#r4").append(data[index].pending_count);
	    	   });
	    	  
	       }
		});    
	});
});


</script>
</head>
<body>
<%
String user= session.getAttribute("UserName").toString();
 if (user != ""){	
%>
<a href="moderator.jsp" style="font-size: 50px;">| Moderator |</a>
<a href="moderatorPublished.jsp" style="font-size: 50px;">| Published Recordings |</a>
<a href="usagestats.jsp" style="font-size: 50px;">| Usage Statistics |</a>
	<h2 style="text-align: center">Moderator</h2>
	<h3 style="text-align: right">
	<span style="text-align: right">User:<%=user%></span>
	</h3>
</div>
<div id="response">
	<table style="border: 1px solid black;  margin: 0 auto;">
		<tr><th>Total number of Messages</th><td id=r1></td></tr>
		<tr><th>Number of Messages Accepted</th><td id=r2></td></tr>
		<tr><th>Number of Messages Rejected</th><td id=r3></td></tr>
		<tr><th>Number of Messages Pending</th><td id=r4></td></tr>
	</table>
</div>
<%}%>
</body>
</html>