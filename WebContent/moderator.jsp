<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="js/jquery-1.11.3.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Review</title>
<style>
body{
	background-image: url("img.png");
}
a:link {
    text-decoration: none;
}
th{
	background-color : rgba(248, 247, 216, 0.7);
}
tr{
	background-color: rgba(248, 247, 216, 0.7);
}
table, td, th {
	
    border: 1px solid green;
}
td {
    padding: 15px;
}
th {
	padding: 20px;
    color: black;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
	$(window).load(function() {
		$.ajax({
	       type : "GET",
	       url : "http://ruralict.cse.iitb.ac.in/Chinchwad/ModeratorReview",
	       success : function(data) {	    	   
	    	   $.each(data,function(index){	    		   
	    		   $("#responseTab").append("<tr id=\"tr"+data[index].id+"\"><td>"+data[index].id+"</td><td> <audio controls=\"controls\"> <source src=\""+data[index].url+"\" type=\"audio/mp3\" /> </audio></td><td>"+data[index].duration+"</td><td id=\"published"+data[index].id+"\">"+data[index].published+"</td><td><button id=\""+data[index].id+"\" onclick=\"btnAccept("+data[index].id+")\">Accept</button><button id=\""+data[index].id+"\" onclick=\"btnReject("+data[index].id+")\">Reject</button></td></tr>");
	    	   });
	    	  
	       }
		});    
	});
});
function btnAccept(btnIndx){
	console.log(btnIndx);
	$.ajax({
       type : "POST",
       url : "http://ruralict.cse.iitb.ac.in/Chinchwad/ModeratorReview",
       data : {"id":btnIndx, "val":"1"},
       success : function(data) {
    	   console.log(data);
    	   if(data == '1'){
    		   console.log("Done");
    		   $("#tr"+btnIndx).css("background-color","#c3fd53");
    		   $("#published"+btnIndx).html("1");
    	   }    	  
       }
	});
}
function btnReject(btnIndx){
	console.log(btnIndx);
	$.ajax({
	       type : "POST",
	       url : "http://ruralict.cse.iitb.ac.in/Chinchwad/ModeratorReview",
	       data : {"id":btnIndx, "val":"-1"},
	       success : function(data) {
	    	   console.log(data);
	    	   if(data == '1'){
	    		   console.log("Done");
	    		   $("#tr"+btnIndx).css("background-color","#ff7f50");
	    		   $("#published"+btnIndx).html("-1");
	    	   }    	  
	       }
		});
}

</script>
</head>
<body>
<%
String user= session.getAttribute("UserName").toString();
if (user != ""){	
%>
<a href="moderator.jsp" style="font-size: 50px;">| Moderator |</a>
<a href="moderatorPublished.jsp" style="font-size: 50px;">| Published Recordings |</a>
<a href="usagestats.jsp" style="font-size: 50px;">| Usage Statistics |</a>
<div width=100%>
	<h2 style="text-align: center">Moderator</h2>
	<h3 style="text-align: right">
	<span style="text-align: right">User: <%=user%></span>
	</h3>
</div>
<div id="response">
	<table style="border: 1px solid black; width:100%;">
		<th>ID</th><th>AUDIO</th><th>DURATION</th><th>PUBLISHED</th><th>ACCEPT/REJECT</th>
		<tbody id="responseTab"></tbody>
	</table>
</div>
<%}%>
</body>
</html>