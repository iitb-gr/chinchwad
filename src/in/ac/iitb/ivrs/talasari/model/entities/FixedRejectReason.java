package in.ac.iitb.ivrs.talasari.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the fixed_reject_reason database table.
 * 
 */
@Entity
@Table(name="fixed_reject_reason")
@NamedQuery(name="FixedRejectReason.findAll", query="SELECT f FROM FixedRejectReason f")
public class FixedRejectReason implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String reason;

	public FixedRejectReason() {
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}