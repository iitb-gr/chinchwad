package in.ac.iitb.ivrs.talasari.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the fixed_message_gender database table.
 * 
 */
@Entity
@Table(name="fixed_message_gender")
@NamedQuery(name="FixedMessageGender.findAll", query="SELECT f FROM FixedMessageGender f")
public class FixedMessageGender implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String gender;

	//bi-directional many-to-one association to MessageGender
	@OneToMany(mappedBy="fixedMessageGender")
	private List<MessageGender> messageGenders;

	public FixedMessageGender() {
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public List<MessageGender> getMessageGenders() {
		return this.messageGenders;
	}

	public void setMessageGenders(List<MessageGender> messageGenders) {
		this.messageGenders = messageGenders;
	}

	public MessageGender addMessageGender(MessageGender messageGender) {
		getMessageGenders().add(messageGender);
		messageGender.setFixedMessageGender(this);

		return messageGender;
	}

	public MessageGender removeMessageGender(MessageGender messageGender) {
		getMessageGenders().remove(messageGender);
		messageGender.setFixedMessageGender(null);

		return messageGender;
	}

}