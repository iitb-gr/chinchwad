package in.ac.iitb.ivrs.talasari.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the moderator_assignment database table.
 * 
 */
@Entity
@Table(name="moderator_assignment")
@NamedQuery(name="ModeratorAssignment.findAll", query="SELECT m FROM ModeratorAssignment m")
public class ModeratorAssignment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="assignment_id")
	private int assignmentId;

	private int accepted;

	@Column(name="reject_reason")
	private String rejectReason;

	//bi-directional many-to-one association to Message
	@ManyToOne
	@JoinColumn(name="message_id")
	private Message message;

	//bi-directional many-to-one association to Moderator
	@ManyToOne
	@JoinColumn(name="moderator_id")
	private Moderator moderator;

	public ModeratorAssignment() {
	}

	public int getAssignmentId() {
		return this.assignmentId;
	}

	public void setAssignmentId(int assignmentId) {
		this.assignmentId = assignmentId;
	}

	public int getAccepted() {
		return this.accepted;
	}

	public void setAccepted(int accepted) {
		this.accepted = accepted;
	}

	public String getRejectReason() {
		return this.rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public Message getMessage() {
		return this.message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public Moderator getModerator() {
		return this.moderator;
	}

	public void setModerator(Moderator moderator) {
		this.moderator = moderator;
	}

}