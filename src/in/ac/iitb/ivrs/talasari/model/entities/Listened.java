package in.ac.iitb.ivrs.talasari.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the listened database table.
 * 
 */
@Entity
@Table(name="listened")
@NamedQuery(name="Listened.findAll", query="SELECT l FROM Listened l")
public class Listened implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="listen_id")
	private int listenId;

	private int liked;

	@Column(name="listening_time")
	private int listeningTime;

	//bi-directional many-to-one association to Call
	@ManyToOne
	@JoinColumn(name="call_id")
	private Call call;

	//bi-directional many-to-one association to Message
	@ManyToOne
	@JoinColumn(name="message_id")
	private Message message;

	public Listened() {
	}

	public int getListenId() {
		return this.listenId;
	}

	public void setListenId(int listenId) {
		this.listenId = listenId;
	}

	public int getLiked() {
		return this.liked;
	}

	public void setLiked(int liked) {
		this.liked = liked;
	}

	public int getListeningTime() {
		return this.listeningTime;
	}

	public void setListeningTime(int listeningTime) {
		this.listeningTime = listeningTime;
	}

	public Call getCall() {
		return this.call;
	}

	public void setCall(Call call) {
		this.call = call;
	}

	public Message getMessage() {
		return this.message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

}