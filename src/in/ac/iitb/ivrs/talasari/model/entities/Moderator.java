package in.ac.iitb.ivrs.talasari.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the moderator database table.
 * 
 */
@Entity
@Table(name="moderator")
@NamedQuery(name="Moderator.findAll", query="SELECT m FROM Moderator m")
public class Moderator implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="user_id")
	private String userId;

	private int active;

	@Column(name="reset_code")
	private int resetCode;

	@Column(name="sha256_password")
	private String sha256Password;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="phone_no")
	private User user;

	//bi-directional many-to-one association to ModeratorAssignment
	@OneToMany(mappedBy="moderator")
	private List<ModeratorAssignment> moderatorAssignments;

	public Moderator() {
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getActive() {
		return this.active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public int getResetCode() {
		return this.resetCode;
	}

	public void setResetCode(int resetCode) {
		this.resetCode = resetCode;
	}

	public String getSha256Password() {
		return this.sha256Password;
	}

	public void setSha256Password(String sha256Password) {
		this.sha256Password = sha256Password;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<ModeratorAssignment> getModeratorAssignments() {
		return this.moderatorAssignments;
	}

	public void setModeratorAssignments(List<ModeratorAssignment> moderatorAssignments) {
		this.moderatorAssignments = moderatorAssignments;
	}

	public ModeratorAssignment addModeratorAssignment(ModeratorAssignment moderatorAssignment) {
		getModeratorAssignments().add(moderatorAssignment);
		moderatorAssignment.setModerator(this);

		return moderatorAssignment;
	}

	public ModeratorAssignment removeModeratorAssignment(ModeratorAssignment moderatorAssignment) {
		getModeratorAssignments().remove(moderatorAssignment);
		moderatorAssignment.setModerator(null);

		return moderatorAssignment;
	}

}