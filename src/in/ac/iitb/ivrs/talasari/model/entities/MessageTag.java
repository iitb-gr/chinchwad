package in.ac.iitb.ivrs.talasari.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the message_tag database table.
 * 
 */
@Entity
@Table(name="message_tag")
@NamedQuery(name="MessageTag.findAll", query="SELECT m FROM MessageTag m")
public class MessageTag implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="message_tag_id")
	private int messageTagId;

	private String tag;

	//bi-directional many-to-one association to Message
	@ManyToOne
	@JoinColumn(name="message_id")
	private Message message;

	//uni-directional many-to-one association to Moderator
	@ManyToOne
	@JoinColumn(name="moderator_id")
	private Moderator moderator;

	public MessageTag() {
	}

	public int getMessageTagId() {
		return this.messageTagId;
	}

	public void setMessageTagId(int messageTagId) {
		this.messageTagId = messageTagId;
	}

	public String getTag() {
		return this.tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public Message getMessage() {
		return this.message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public Moderator getModerator() {
		return this.moderator;
	}

	public void setModerator(Moderator moderator) {
		this.moderator = moderator;
	}

}