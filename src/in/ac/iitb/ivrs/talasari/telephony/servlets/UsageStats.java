package in.ac.iitb.ivrs.talasari.telephony.servlets;

import in.ac.iitb.ivrs.talasari.model.EntityManagerService;
import in.ac.iitb.ivrs.talasari.model.entities.Message;

import java.io.IOException;
import java.util.List;

import javax.management.Query;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class ModeratorReview
 */
@WebServlet("/UsageStats")
public class UsageStats extends HttpServlet {		//log(request.getParameter("id"));

	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UsageStats() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JSONArray jsonArr = new JSONArray();
		EntityManager em = EntityManagerService.getEntityManager();
		
		List<Message> message=em.createNamedQuery("Message.message_total", Message.class).getResultList();
		int accept_count=0,reject_count=0,pending_count=0;
		
		for (int i=0;i<message.size();i++){
			
	
				if(message.get(i).getPublished()==1)
					accept_count++;
				else if(message.get(i).getPublished()==-1)
					reject_count++;
				else if(message.get(i).getPublished()==0)
					pending_count++;
		
		
		}
		
		System.out.println("total,accept,reject,pending counts are "+message.size()+" "+accept_count+" "+reject_count+" "+pending_count);
		JSONObject jsonObj= new JSONObject();
		try {
			jsonObj.put("total_count", message.size());
			jsonObj.put("accept_count", accept_count);
			jsonObj.put("reject_count", reject_count);
			jsonObj.put("pending_count", pending_count);
			jsonArr.put(jsonObj);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		response.setContentType("application/json");
	
		response.getWriter().write(jsonArr.toString());
		
		//response.sendRedirect("index.jsp?message="+message);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//log(request.getParameter("id"));
		System.out.println(request.getParameter("id")+" "+request.getParameter("val"));
		EntityManager em = EntityManagerService.getEntityManager();	
		try{
			TypedQuery<Message> query = em.createNamedQuery("Message.updatePublished", Message.class);
			query.setParameter("id", Integer.parseInt(request.getParameter("id")));
			query.setParameter("val", Integer.parseInt(request.getParameter("val")));
			int upd = query.executeUpdate();
			System.out.println("upd"+upd);
			response.setContentType("text/plain");
			response.getWriter().write(upd+"");
		}
		catch(Exception e){
			System.out.println("e"+e);
		}
	}

}
