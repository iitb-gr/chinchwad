package in.ac.iitb.ivrs.talasari.telephony.fsm.actions;

import in.ac.iitb.ivrs.talasari.model.EntityManagerService;
import in.ac.iitb.ivrs.talasari.telephony.TalasariSession;
import in.ac.iitb.ivrs.telephony.base.IVRSession;

import javax.persistence.EntityManager;

import com.continuent.tungsten.commons.patterns.fsm.Action;
import com.continuent.tungsten.commons.patterns.fsm.Event;
import com.continuent.tungsten.commons.patterns.fsm.Transition;
import com.continuent.tungsten.commons.patterns.fsm.TransitionFailureException;
import com.continuent.tungsten.commons.patterns.fsm.TransitionRollbackException;

/**
 * Action to perform when saving a recorded message.
 */
public class DoSaveMessageAction implements Action<IVRSession> {

	@Override
	public void doAction(Event<?> event, IVRSession session, Transition<IVRSession, ?> transition, int actionType)
			throws TransitionRollbackException, TransitionFailureException {

		TalasariSession talasariSession = (TalasariSession) session;
		EntityManager em = EntityManagerService.getEntityManager();
		em.persist(talasariSession.getRecordedMessage());
	}

}
