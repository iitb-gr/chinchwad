package in.ac.iitb.ivrs.talasari.telephony.fsm.actions;

import in.ac.iitb.ivrs.talasari.config.Configs;
import in.ac.iitb.ivrs.talasari.telephony.TalasariSession;
import in.ac.iitb.ivrs.telephony.base.IVRSession;

import com.continuent.tungsten.commons.patterns.fsm.Action;
import com.continuent.tungsten.commons.patterns.fsm.Event;
import com.continuent.tungsten.commons.patterns.fsm.Transition;
import com.continuent.tungsten.commons.patterns.fsm.TransitionFailureException;
import com.continuent.tungsten.commons.patterns.fsm.TransitionRollbackException;
import com.ozonetel.kookoo.Response;

/**
 * Action that replays the recorded message.
 */
public class PlayRecordedMessageAction implements Action<IVRSession> {

	@Override
	public void doAction(Event<?> event, IVRSession session, Transition<IVRSession, ?> transition, int actionType)
			throws TransitionRollbackException, TransitionFailureException {

		TalasariSession talasariSession = (TalasariSession) session;
		Response response = session.getResponse();

		//response.addPlayText("Your message is:", Configs.Telephony.TTS_SPEED);
		response.addPlayAudio(Configs.Voice.VOICE_DIR + "/yourMessageIs.wav");

		response.addPlayAudio(talasariSession.getRecordedMessage().getMessageUrl());
	}

}
